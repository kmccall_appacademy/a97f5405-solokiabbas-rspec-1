def translate(string)
    result=string.split(" ")
    result=result.map do |str|
        singletrans(str).to_s+" "
    end
    result.join("").strip

end

def singletrans(string)
    punct=''
    vowels=["a","e","i","o","u"]
    alphabet=['a'..'z'].to_a
    consonants= alphabet - vowels
    if string.count(',')>0
        string=removepunc(string)
        punct=','
    end
    if string==string.capitalize
        return singletrans(string.downcase).capitalize
    else
        if vowels.include?(string[0])
            return string+'ay'+punct
        elsif consonants.include?(string[0])&& consonants.include?(string[1])
            return string[2..-1]+string[0..1]+'ay'+punct
        elsif string[0..1] == "qu"
            return string[2..string.length]+"quay"+punct
        elsif string[0..2] == "squ"
            return string[3..string.length]+"squay"+punct
        elsif string[0..2] == "sch"
            return string[3..-1] + "schay"+punct
        elsif string[0..2] == "thr"
            return string[3..-1] + "thray"+punct
        elsif string[0..1] == "ch"
            return string[2..-1] + "chay"+punct
        elsif string[0..1] == "th"
            return string[2..-1] + "thay"+punct
        elsif string[0..1] == "br"
            return string[2..-1] + "bray"+punct
        else consonants.include?(string[0])
            return string[1..-1] + string[0..0] + 'ay'+punct
        end
    end
end

def removepunc(string)
    temp=''
    string.each_char do |char|
        temp+=char unless char==','
    end
    return temp
end

#puts removepunc("theox,")
