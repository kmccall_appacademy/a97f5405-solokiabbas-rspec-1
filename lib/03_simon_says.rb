def echo(string)
    string
end

def shout(string)
    string.upcase
end

def repeat(string, int=2)
    ("#{string} "*int).strip
end

def start_of_word(string, int)
    string[0...int]
end

def first_word(string)
    string.split(" ")[0]
end

def titleize(string)
    exclude=["the","in", "of", "and", "a", "an", "over"]
    @first=string.split(" ").map{|cap| unless exclude.include? (cap)
        cap.capitalize
        else
          cap
          end }
           @first[0]=@first[0].capitalize
          @first.join(" ")
end
