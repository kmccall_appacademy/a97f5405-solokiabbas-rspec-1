def add(int1,int2)
    int1+int2
end

def subtract(int1,int2)
    int1-int2
end

def sum(ary=[])
    @sum=0
    ary.each {|ar| @sum+=ar}
    @sum
end

def multiply(ary=[])
    @multiply=1
    ary.each {|ar| @multiply*=ar}
    @multiply
end

def power(int1,int2)
    int1**int2
end

def factorial(int)
    return int*factorial(int-1) if int>1
    return 1 if int==0 || int==1
end
